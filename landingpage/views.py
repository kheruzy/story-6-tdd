from django.shortcuts import render, redirect
from .models import Story
from . import forms

# Create your views here.

def landingpage(request):
    if request.method == 'POST':
        form = forms.CreateStory(request.POST)
        if form.is_valid():
            form.save()
            return redirect('landingpage:landingpage')
    else:
        form = forms.CreateStory()

    stories = Story.objects.all().order_by('time')
    return render(request, 'index.html', {'form': form, 'stories': stories})

def delete_story(request):
    if request.method == "POST":
        id = request.POST['id']
        Story.objects.get(id=id).delete()
    return redirect('landingpage:landingpage')