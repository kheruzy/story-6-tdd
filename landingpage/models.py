from django.db import models

# Create your models here.
class Story(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField(blank=True, max_length=300)
    time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
